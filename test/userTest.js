const assert = require('chai').assert;
const User = require('../models/user');

// Testing inputs

describe('Create User', function () {
    it('App should create a User sucessfully', function () {
        User.create('Julio Teste', 'teste@mail.com', new Date(1995, 10, 12), 'Rua Teste', 933333333, 'utilizador', 'abc123');
        User.findOne({ email: 'teste@mail.com' }).then(user => {
            assert.equal(user.name, 'Julio Teste', 'User was not created');
        });
    });
});

describe('Update User', function () {
    it('App should update a User sucessfully', function () {
        User.create('Julio Teste', 'teste@mail.com', new Date(1995, 10, 12), 'Rua Teste', 933333333, 'utilizador', 'abc123');
        const updatedUser = {
            name: 'Julio Teste atualizado',
            email: 'teste@mail.com',
            password: 'abc123',
            birthdate: new Date(1995, 10, 12),
            address: 'Rua Teste',
            phone: 933333333,
            role: 'utilizador'
        };
        User.update(updatedUser.email, updatedUser.name, updatedUser.birthdate, updatedUser.address, updatedUser.phone);
        User.findOne({ email: updatedUser.email }).then(user => {
            assert.equal(user, updatedUser, 'User was not updated');
        });
    });
});

describe('Delete User', function () {
    it('App should delete a user', function(){
        userEmail = 'teste@mail.com';
        User.create('Julio Teste', 'teste@mail.com', new Date(1995, 10, 12), 'Rua Teste', 933333333, 'utilizador', 'abc123');
        User.delete(userEmail);
        User.findOne({email: userEmail}).then(user => {
            assert.notExists(user, 'User not deleted');
        });
    });
});

// Testing errors in inputs

describe('User not existant', function(){
    it('App should recieve an error for not existant user', function(){
        User.findOne({email: 'nao existe'}).then(user => {
            assert.notExists(user, 'User exists but it shouldnt');
        });
    });
});

describe('User creation failed', function(){
    it('App should fail to create a user with invalid parameters', function(){
        User.create(123, 'test', null, 'fake street', 933111, 'utilizador', 'abc');
        User.findOne({email: 'test'}).then(user => {
            assert.notExists(user, 'User created and it shouldnt have been');
        });
    });
});

describe('User update failed', function(){
    it('App should fail to update a user with invalid parameters', function(){
        const userEmail = 'teste@mail.com';
        User.create('Julio Teste', userEmail, new Date(1995, 10, 12), 'Rua Teste', 933333333, 'utilizador', 'abc123');
        const updatedUser = {
            name: 'Julio Teste atualizado',
            email: userEmail,
            password: 'abc123',
            birthdate: null,
            address: 'Rua Teste',
            phone: 9333,
            role: 'utilizador'
        };
        User.update(updatedUser.email, updatedUser.name, updatedUser.birthdate, updatedUser.address, updatedUser.phone);
        User.findOne({ email: userEmail }).then(user => {
            assert.notEqual(user, updatedUser, 'User was updated');
        });
    });
});

describe('User update failed', function(){
    it('App should fail to update a user with non existant email', function(){
        userEmail = 'teste@mail.com';
        User.create('Julio Teste', userEmail, new Date(1995, 10, 12), 'Rua Teste', 933333333, 'utilizador', 'abc123');
        const updatedUser = {
            name: 'Julio Teste atualizado',
            email: 'teste',
            password: 'abc123',
            birthdate: new Date(1995, 10, 12),
            address: 'Rua Teste',
            phone: 933333333,
            role: 'utilizador'
        };
        User.update(updatedUser.email, updatedUser.name, updatedUser.birthdate, updatedUser.address, updatedUser.phone);
        User.findOne({ email: userEmail }).then(user => {
            assert.notEqual(user, updatedUser, 'User was updated');
        });
    });
});

// routing tests

