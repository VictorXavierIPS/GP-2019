const assert = require('chai').assert;
const Animal = require('../models/animal');


// Testing inputs

describe('Create Animal for adoption', function () {
    it('App should create an Animal for adoption sucessfully', function () {
        Animal.create('small', 'small', 'Golden Retriever', new Date(2005, 10, 12), 'animal teste', 'male', 'desc', 'veryactive', 'sociable', true, true, true, 'high', 'very', true, null);
        Animal.findOne({ name: 'animal teste' }).then(animal => {
            assert.equal(animal.name, 'animal teste', 'Animal was not created');
            Animal.delete(animal._id);
        });
    });
});

describe('Update Animal for adoption', function () {
    it('App should update an Animal for adoption sucessfully', function () {
        Animal.create('small', 'small', 'Golden Retriever', new Date(2005, 10, 12), 'animal teste', 'male', 'desc', 'veryactive', 'sociable', true, true, true, 'high', 'very', true, null);
        const updatedAnimal = {
            fur: 'small',
            size: 'small',
            breed: 'Golden Retriever',
            adopted: false,
            birthdate: new Date(2005, 10, 12),
            name: 'animal teste atualizado',
            gender: 'male',
            description: 'desc',
            behavior: 'veryactive',
            sociability: 'sociable',
            dogFriendly: true,
            catFriendly: true,
            childFriendly: true,
            inteligent: 'high',
            trainable: 'very',
            watchdog: true,
            picture: null
        };
        Animal.findOne({name: 'animal teste'}).then(animal => {
            Animal.update(animal._id, updatedAnimal.fur, updatedAnimal.size, updatedAnimal.breed, updatedAnimal.adopted, updatedAnimal.birthdate, updatedAnimal.name, updatedAnimal.gender, updatedAnimal.description, updatedAnimal.behavior, updatedAnimal.sociability, updatedAnimal.dogFriendly, updatedAnimal.catFriendly, updatedAnimal.childFriendly, updatedAnimal.inteligent, updatedAnimal.trainable, updatedAnimal.watchdog, updatedAnimal.picture);
        });
        Animal.findOne({ name: updatedAnimal.name }).then(animal => {
            assert.equal(animal, updatedAnimal, 'Animal was not updated');
            Animal.delete(animal._id);
        });
    });
});

describe('Delete Animal for adoption', function () {
    it('App should delete an Animal for adoption', function () {
        animalName = 'animal teste';
        Animal.create('small', 'small', 'Golden Retriever', new Date(2005, 10, 12), 'animal teste', 'male', 'desc', 'veryactive', 'sociable', true, true, true, 'high', 'very', true, null);
        Animal.findOne({ name: animalName }).then(animal => {
            Animal.delete(animal._id);
        });
        Animal.findOne({ name: animalName }).then(animal => {
            assert.notExists(animal, 'User not deleted');
        });
    });
});
