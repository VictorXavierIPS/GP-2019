const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const passport = require('passport');
const flash = require('connect-flash');
const session = require('express-session');
const User = require('./models/user');
const Breed = require('./models/breed');
const bcrypt = require('bcryptjs');
const traits = require('./models/traits.json');

const app = express();

var path = require('path');

// Passport Config
require('./config/passport')(passport);


const PORT = process.env.PORT || 8081;


// DB Config
const db = 'mongodb+srv://dbUser:user@cluster0-epch2.gcp.mongodb.net/test?retryWrites=true';



// Connect to MongoDB
mongoose
  .connect(
    db,
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

// EJS
app.use(expressLayouts);
app.set('view engine', 'ejs');


// Express body parser
app.use(express.urlencoded({ extended: true }));

// Express session
app.use(
  session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
  })
);

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Connect flash
app.use(flash());

// Global variables
app.use(function (req, res, next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
});


User.findOne({ email: 'gestaop.19@gmail.com' }).then(user => {
  if (user) {

  } else {
    const admin = new User({
      name: 'Administrador',
      email: 'gestaop.19@gmail.com',
      password: 'albergue',
      birthdate: '1998-04-27T00:00:00.000+00:00',
      address: undefined,
      phone: undefined,
      role: "admin",
      confirmed: true
    });


    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(admin.password, salt, (err, hash) => {
        if (err) throw err;
        admin.password = hash;
        admin.save().catch(err => console.log(err))
      });
    });
  }
});

Breed.findOne({ name: 'Indefinida' }).then(breed => {
  if (breed) { } else {
    const breed = new Breed({
      name: 'Indefinida'
    }).save();
  }
}
);

for (const key in traits) {
  Breed.findOne({ name: key }).then(breed => {
    if (breed) { } else {
      const breed = new Breed({
        name: key
      }).save();
    }
  }
  );
}


// Routes
app.use('/', require('./routes/index.js'));
app.use('/users', require('./routes/users.js'));
app.use('/adoption', require('./routes/adoption.js'));
app.use('/animals', require('./routes/animals.js'));
app.use('/idealAnimal', require('./routes/idealAnimal.js'));
app.use('/similarBreed', require('./routes/similarBreed.js'));
app.use(express.static(path.join(__dirname, 'public')));

app.get("/healthz", (req, res) => {
  res.json({ healthz: "good" });
});


app.listen(PORT, console.log(`Server started on port ${PORT}`));
