const express = require('express');
const router = express.Router();

const Animal = require('../models/animal');
const traits = require('../models/traits.json');

router.get('/', (req, res) => res.render('similarBreed/similarBreed', { user: req.user }));

router.post('/similarBreed', (req, res) => {
    const breed = req.body.breed;
    const breedTraits = traits[breed];
    var animalMostCommon = [];
    var animalSameBreed = [];
    Animal.find({}, function(err, animals){
        animals.forEach(function(animal){
            var commonPoints = 1;
            if(animal.breed == breed){
                animalSameBreed.push(animal);
            }else{
                if(breedTraits['Friendly']['dogs'] != null && animal.dogFriendly == breedTraits['Friendly']['dogs']){
                    commonPoints += 1;
                }
                if(breedTraits['Friendly']['cats'] != null && animal.catFriendly == breedTraits['Friendly']['cats']){
                    commonPoints += 1;
                }
                if(breedTraits['Friendly']['children'] != null && animal.childFriendly == breedTraits['Friendly']['children']){
                    commonPoints += 1;
                }
                if(breedTraits['Inteligent'] != null && animal.inteligent == breedTraits['Inteligent']){
                    commonPoints += 1;
                }
                if(breedTraits['Trainable'] != null && animal.trainable == breedTraits['Trainable']){
                    commonPoints += 1;
                }
                if(breedTraits['Watchdog'] != null && animal.watchdog == breedTraits['Watchdog']){
                    commonPoints += 1;
                }
                if(breedTraits['Playful'] != null && animal.behavior == "playful" && breedTraits['Playful']){
                    commonPoints += 1;
                }
                if(commonPoints > 4){
                    animalMostCommon.push(animal);
                }
            }
        });
        var animalList = animalSameBreed.concat(animalMostCommon);
        setTimeout(function () { res.render('similarBreed/similarBreedList', {'list': animalList, user: req.user }); }, 220 * animals.length);
    });
});


module.exports = router;