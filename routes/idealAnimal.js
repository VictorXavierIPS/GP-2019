const express = require('express');
const router = express.Router();
const { ensureAuthenticated } = require('../config/auth');

const Animal = require('../models/animal');

// Welcome Page
router.get('/', (req, res) => res.render('idealAnimal/idealAnimal', { user: req.user }));

router.post('/animals', (req, res) => {
    const { question1, question2, question3, question4, question5, question6, question7 } = req.body
    var idealAnimals = []
    Animal.find({}, function (err, animals) {
        animals.forEach(animal => {
            var points = 0;
            switch (question1) {
                case "Muito Ativa":
                    if (animal.behavior == "veryactive" || animal.behavior == "playful") {
                        points += 1
                    }
                    break;
                case "Ativa":
                    if (animal.behavior == "active" || animal.behavior == "playful") {
                        points += 1
                    }
                    break;
                case "Calma":
                    if (animal.behavior == "calm") {
                        points += 1
                    }
                    break;
                case "Muito Calma":
                    if (animal.behavior == "verycalm") {
                        points += 1
                    }
                    break;
            }

            switch (question2) {
                case "Muito Espaço":
                    if (animal.size == "large") {
                        points += 1
                    }
                    break;
                case "Espaço Suficiente":
                    if (animal.size == "medium") {
                        points += 1
                    }
                    break;
                case "Pouco Espaço":
                    if (animal.size == "small") {
                        points += 1
                    }
                    break;
            }

            switch (question3) {
                case "Muito Social":
                    if (animal.sociability == "sociable") {
                        points += 1
                    }
                    break;
                case "Relativamente Social":
                    if (animal.sociability == "shy") {
                        points += 1
                    }
                    break;
                case "Pouco Social":
                    if (animal.sociability == "aggressive") {
                        points += 1
                    }
                    break;
            }

           

            switch (question5) {
                case "Campo":
                    if (animal.size == "large" || animal.size == "medium") {
                        points += 1
                    }
                    break;
                case "Cidade":
                    if (animal.size == "medium" || animal.size == "small") {
                        points += 1
                    }
                    break;
            }

            switch (question6) {
                case "Sim":
                    if (animal.childFriendly ==  true && animal.sociability != "aggressive") {
                        points += 1
                    }
                    break;
                case "Não":
                    if (animal.behavior == "verycalm" || animal.behavior == "calm") {
                        points += 1
                    }
                    break;
            }
            switch (question7) {
                case "Companhia":
                    if (animal.behavior == "calm" || animal.behavior == "verycalm") {
                        points += 1
                    }
                    break;
                case "Segurança":
                    if (animal.watchdog == true) {
                        points += 1
                    }
                    break;
                case "Mais Ativo":
                    if (animal.behavior == "active" || animal.behavior == "veryactive") {
                        points += 1
                    }
                    break;
                case "Companhia Criança":
                    if ((animal.behavior == "active" || animal.behavior == "veryactive" || animal.behavior == "playful") && animal.sociability != "aggresive") {
                        points += 1
                    }
                    break;
            }

            switch (question4) {
                case "Macho":
                    if (animal.gender == "male") {
                        points += 1
                        if(animal.adopted == false){
                        idealAnimals.push({ animal: animal, points: points });
                        }
                    }
                    break;
                case "Fêmea":
                    if (animal.gender == "female") {
                        points += 1
                        if(animal.adopted == false){
                            idealAnimals.push({ animal: animal, points: points });
                            }
                    }
                    break;
                case "Indiferente":
                    points += 0
                    if(animal.adopted == false){
                        idealAnimals.push({ animal: animal, points: points });
                        }
                    break;
            }
            
        })
        idealAnimals.sort(function (a, b) {
            if (a.points > b.points) {
              return -1;
            }
            if (a.points < b.points) {
              return 1;
            }
            // a must be equal to b
            return 0;
          })
        res.render('idealAnimal/idealAnimalsList', { user: req.user, 'list': idealAnimals.slice(0,9) });
    });



});


router.get('/denied', (req, res) => res.render('accessdenied', { user: req.user }));

module.exports = router;
