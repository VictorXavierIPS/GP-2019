/**
 * This file will deal with all the routing actions of the users, creating, 
 * updating roles, deleting and other auxiliary function
 */

const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
const age = require('../config/age');
const sgMail = require('@sendgrid/mail');

// Load User model
const User = require('../models/user');

sgMail.setApiKey('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');

/**
 * This will render the login page
 */
router.get('/login', (req, res) => res.render('login', { user: req.user }));

/**
 * This function will generate a random password 
 * 
 * @param {Number} length 
 */
function makePassword(length) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < length; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
};

/**
 * This will render the password restoration page
 */
router.get('/restorePassword', (req, res) => res.render('restorePassword', { user: req.user }));

/**
 * This will save a new password to the user that wants to restore its password
 * and an email will be sent to the user with the new password
 */
router.post('/restorePassword', (req, res) => {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario" || req.user.role == "utilizador")) {
    const email = req.body.email;
    if (!email) {
      req.flash('error_msg', 'Por favor insira o seu email'
      );
    }
    else {
      User.findOne({ email: email }).then(user => {
        if (user) {
          var pw = makePassword(7);
          var encPw = '';
          bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(pw, salt, (err, hash) => {
              if (err) throw err;
              encPw = hash;
              user.updateOne({ password: encPw }, function (err) {
                if (err) {
                  console.log('Erro a encontrar utilizador!');
                } else {
                  //alterar passe 
                  const msg = {
                    to: email,
                    from: 'noreplay@example.com',
                    subject: 'Esqueceu-se da Palavra-Passe',
                    html: '<img src="../public/images/logotipo.png" alt="logo"><br><br>O seu pedido de recuperação de palavra-passe foi realizado com sucesso.<br>Foi-lhe <strong>atribuida uma nova palavra-passe</strong> por questões de segurança.<br>A sua nova palavra-passe é <strong>' + pw + '</strong><br>Caso pretenda alterar a palavra-passe que lhe foi atribuida para uma que lhe seja mais fácil de memorizar apenas terá que se <strong>Auntenticar</strong>, ir ao <strong>Perfil</strong> e escolher a opção <strong>Alterar Palavra-Passe</strong>.<br><br>RELEMBRAMOS NÃO COMPRE, ADOPTE<br><u>QUINTA DO MIÃO</u>',
                  };
                  sgMail.send(msg);
                }
              });
            });
          });
        }
      });
    }
    req.flash(
      'success_msg',
      'Verifique o seu correio eletrónico'
    );
    res.redirect('/users/login');
  }
});

/**
 * This will render the registration page
 */
router.get('/register', (req, res) => res.render('register', { user: req.user }));

/**
 * This will render the personal account page of the user, this is only accessible
 * to every kind of user that is logged in.
 */
router.get('/account', function (req, res) {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario" || req.user.role == "utilizador")) {
    res.render("account", { user: req.user });
  } else {
    res.redirect('/denied');
  }

});


/**
 * This will check for errors in the inputs of the register form, it will create 
 * the user and send an email for the user to confirm their account
 */
router.post('/register', (req, res) => {
  const { name, email, birthdate, address, phone, role, password, password2 } = req.body;
  let errors = [];

  if (!name || !email || !birthdate || !password || !password2) {
    errors.push({ msg: 'Por favor preencha todos os campos obrigatórios' });
  }
  if (phone !== '' && phone.length < 9) {
    errors.push({ msg: 'Telemóvel deve ter no mínimo 9 dígitos' });
  }
  if (age.age(birthdate) < 16) {
    errors.push({ msg: 'Deve ter mais de 16 anos para se poder registar' });
  }
  if (password != password2) {
    errors.push({ msg: 'Confirmação diferente da palavra-passe' });
  }

  if (password.length < 6) {
    errors.push({ msg: 'Palavra-passe deve ter pelo menos 6 caracteres' });
  }

  if (errors.length > 0) {
    res.render('register', {
      errors,
      name,
      email,
      birthdate,
      address,
      phone,
      role,
      password,
      password2,
      user: req.user
    });
  } else {
    User.findOne({ email: email }).then(user => {
      if (user) {
        errors.push({ msg: 'Este email já está em uso' });
        res.render('register', {
          errors,
          name,
          email,
          birthdate,
          address,
          phone,
          password,
          password2,
          user: req.user
        });
      } else {
        const newUser = new User({
          name,
          email,
          password,
          birthdate,
          address,
          phone,
          role: 'utilizador'
        });

        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser
              .save()
              .then(user => {
                req.flash(
                  'success_msg',
                  'Registo feito com sucesso, verifique o seu email antes de realizar o log in'
                );
                sgMail.setApiKey('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');
                const msg = {
                  to: email,
                  from: 'noreplay@example.com',
                  subject: 'Confirmação de Email',
                  html: '<img src="../public/images/logotipo.png" alt="logo"><br><br>O seu registo na plataforma Quinta do Mião foi realizado com sucesso.<br>De modo a disfrutar de todas as funcionalidades da nossa plataforma, clique <a href="http://35.242.140.31:30006/users/sucessRegistrationConfirmation/' + email + '">aqui</a>.<br><br>RELEMBRAMOS NÃO COMPRE, ADOPTE<br><u>QUINTA DO MIÃO</u>',
                };
                sgMail.send(msg);
                res.redirect('/users/login');
                console.log("Email de confirmação de registo enviado...");
              })
              .catch(err => console.log(err));
          });
        });

      }
    });
  }
});


/**
 * This will authenticate the user in the platform
 */
router.post('/login', (req, res, next) => {
  passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/users/login',
    failureFlash: true
  })(req, res, next);
});

/**
 * This will render the homepage with the user not authenticated
 */
router.get('/logout', (req, res) => {
  req.logout();
  req.flash('success_msg', 'Deixou de estar Autenticado');
  res.redirect('/users/login');
});

/**
 * This will render the list of users and can only be accessed by the admin account
 */
router.get('/usersList', function (req, res) {
  if (req.user != null && req.user.role == "admin") {
    User.find({}, function (err, users) {
      var userMap = [];
      users.forEach(function (user) {
        userMap.push(user);
      });

      res.render('userslist', { 'list': userMap, user: req.user });
    });
  } else {
    res.redirect('/denied');
  }

});

/**
 * This will render the page where its possible to change the role of a user
 */
router.get('/role/:email', (req, res) => {
  res.render('roleChange', { user: req.user, email: req.params.email });
});

/**
 * This will update the selected users role to the role selected by the
 * admin account and it will send an email to the user that got his role changed
 */
router.post('/role', (req, res) => {
  if (req.user != null && (req.user.role == "admin")) {
    User.findOne({ email: req.body.email }, function (err, user) {
      if (err) throw err;
      user.updateOne({ role: req.body.role }, function (err) {
        if (err) {
          console.log('Erro a atualizar!');
          res.redirect('userslist');
        } else {
          console.log('Sucesso a atualizar!');
          sgMail.setApiKey('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');
          const msg = {
            to: user.email,
            from: 'noreplay@example.com',
            subject: 'Mudança de cargo',
            html: `<img src="../views/images/miao.png" alt="logo"><br><br>O seu cargo foi alterado para ${user.role}.<br><br>RELEMBRAMOS NÃO COMPRE, ADOPTE<br><u>QUINTA DO MIÃO</u>`,
          };
          sgMail.send(msg);
          res.redirect('userslist');
        }
      });
    });
  }
});

/**
 * This will update the user with the new values inserted, this is only done by the 
 * user itself that feels the need to change some information on their profile
 */
router.post('/updateUser', function (req, res) {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario" || req.user.role == "utilizador")) {
    User.findOne({ email: req.user.email }).then(user => {
      var newvalues = { $set: { name: req.body.name, birthdate: req.body.birthdate, address: req.body.address, phone: req.body.contact } };

      User.findOneAndUpdate({ email: req.user.email }, newvalues, { new: true }, function (err, res) {
        if (err) throw err;
        console.log(user._id);
        console.log(req.body);
      });
      var userUpdated = { name: req.body.name, birthdate: req.body.birthdate, address: req.body.address, phone: req.body.contact };

      res.render("account", { user: userUpdated });
      req.flash('success_msg', 'As suas alterções foram salvas');

    });
  } else {
    res.redirect('/denied');
  }
});

/**
 * This will update the password of the user and sending an email to the user confirming
 * the change
 */
router.post('/updatePassword', function (req, res) {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario" || req.user.role == "utilizador")) {
    User.findOne({ email: req.user.email }, function (err, user) {
      if (err) throw err;
      var pw = req.body.password;
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(pw, salt, (err, hash) => {
          if (err) throw err;
          pw = hash;
          user.updateOne({ password: pw }, function (err) {
            if (err) {
              console.log('Erro a atualizar!');
              res.redirect('account');
            } else {
              console.log('Sucesso a atualizar!');
              sgMail.setApiKey('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');
              const msg = {
                to: user.email,
                from: 'noreplay@example.com',
                subject: 'Password Alterada',
                html: `<img src="../views/images/miao.png" alt="logo"><br><br>A sua password foi alterada com sucesso.<br><br>RELEMBRAMOS NÃO COMPRE, ADOPTE<br><u>QUINTA DO MIÃO</u>`,
              };
              sgMail.send(msg);
              res.redirect('account');
            }
          });
        });
      });
    });
  } else {
    res.redirect('/denied');
  }
});

//Confirm Registration
router.get('/sucessRegistrationConfirmation/:email', function (req, res) {
  User.findOne({ email: req.params.email }, function (err, user) {
    if (err) throw err;
    user.updateOne({ confirmed: true }, function (err) {
      if (err) {
        console.log('Erro a confirmar mail!');
        res.redirect('/users/login');
      } else {
        console.log('email confirmado!');
        res.redirect('/users/login');
      }
    });
  });
});


router.get('/delete/:id', function (req, res) {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
    User.findByIdAndRemove({ _id: req.params.id }).then(animal => {
      console.log(req.params.id);
      req.flash(
        'success_msg',
        'Ficha eliminada com sucesso'
      );
      res.redirect('/users/userlist');
    });
  } else {
    res.redirect('/denied');
  }
});
module.exports = router;

