const express = require('express');
const router = express.Router();
const { ensureAuthenticated } = require('../config/auth');


// Welcome Page
router.get('/', (req, res) => res.render('welcome', {user: req.user}));


router.get('/denied', (req, res) => res.render('accessdenied', {user: req.user}));

module.exports = router;
