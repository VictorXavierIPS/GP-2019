const express = require('express');
const router = express.Router();
const age = require('../config/age');
const sgMail = require('@sendgrid/mail');

// Load User and Adoption model
const Adoption = require('../models/adoptionFile');
const AdoptionPreConditions = require('../models/adoptionPreCondition');
const Animal = require('../models/animal')
const User = require('../models/user');

// Show Create Page
router.get('/create', (req, res) => {
  if (req.user != null && req.user.role == "admin" || req.user.role == "funcionario") {
    res.render('adoption/adoptionCreate', { user: req.user, minDate: new Date().toISOString().substring(0, 10) });
  } else {
    res.redirect('/denied');
  }
});

// Create adoption 
router.post('/create', (req, res) => {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario" || req.user.role == "utilizador")) {
    const { animal, userAdopt, userWorker, adoptionDate, adoptionStatus } = req.body;
    let errors = [];
    if (!animal || !userAdopt || !userWorker || !adoptionDate) {
      errors.push({ msg: 'Por favor preencha todos os campos obrigatórios' });
    }

    if (errors.length > 0) {
      res.render('adoption/adoptionCreate', {
        errors,
        animal, userAdopt, userWorker, adoptionDate, adoptionStatus,
        user: req.user, minDate: new Date().toISOString().substring(0, 10)
      });
    } else {
      const newAdoption = new Adoption({
        animal, userAdopt, userWorker, adoptionDate, adoptionStatus: "pendente"
      });
      newAdoption
        .save()
        .then(adoption => {
          res.redirect('/');
        })
        .catch(err => console.log(err));
      // }
    }//);
  }
  else {
    res.redirect('/denied');
  }
});

//show adoptions list page
router.get('/adoptionList', function (req, res) {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
    var adoptMap = [];
    Adoption.find({}, function (err, adoptions) {

      adoptions.forEach(function (adopt) {
        var ado = { adopt: adopt, animalName: null, userAdoptName: null };

        Animal.findOne({ _id: adopt.animal }, function (err, animal) {
          ado.animalName = animal.name;
          User.findOne({ email: adopt.userAdopt }, function (err, user) {
            ado.userAdoptName = user.name;
            User.findOne({ _id: adopt.userWorker }, function (err, user) {
              if (user) {
                ado.userWorkerName = user.name;
              }
              adoptMap.push(ado);
            });
          });
        });
      })
      setTimeout(function () { res.render('adoption/adoptionList', { 'list': adoptMap, user: req.user }); }, 250 * adoptions.length);
    });
  } else {
    res.redirect('/denied');
  }
});

//Delete adoption
router.get('/delete/:id', function (req, res) {
  if (req.user != null && req.user.role == "admin" || req.user.role == "funcionario") {
    Adoption.findOne({ _id: req.params.id }, function (err, adoption) {
      AdoptionPreConditions.findByIdAndDelete({ _id: adoption.preform }, function (err, pre) {
        Adoption.findByIdAndRemove({ _id: req.params.id }, function (err, ado) {
          req.flash(
            'success_msg',
            'Adoção removida com sucesso'
          );
          res.redirect('/adoption/adoptionList');
        })
      });

    });
  } else {
    res.redirect('/denied');
  }
});

//show update adoption page
router.get('/updateAdoption/:id', function (req, res) {
  if (req.user != null && req.user.role == "admin" || req.user.role == "funcionario") {
    Adoption.findById({ _id: req.params.id }).then(adopt => {
      res.render('adoption/adoptionUpdate', {
        user: req.user, adopt: adopt, adoptDate: new Date(adopt.adoptionDate).toISOString().substring(0, 10)
        , minDate: new Date().toISOString().substring(0, 10)
      })
    });
  } else {
    res.redirect('/denied');
  }
});

//update adoption
router.post('/update', function (req, res) {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
    const { adoptId, animal, userAdopt, userWorker, adoptionDate } = req.body;
    var newvalues = { $set: { animal: animal, userAdopt: userAdopt, userWorker: userWorker, adoptionDate: adoptionDate } };
    Adoption.findOneAndUpdate({ _id: adoptId }, newvalues, { new: true }, function (err, ado) {
      if (err) {
        req.flash(
          'error_msg',
          'Erro a atualizar a adoção'
        );
        res.redirect('/adoption/update/' + adoptId);
      } else {
        req.flash(
          'success_msg',
          'Adoção removida com sucesso'
        );
        res.redirect('/adoption/adoptionList');
      }
    });
  } else {
    res.redirect('/denied');
  }
});



//update status accept adoption
router.post('/update/accept', function (req, res) {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
    const { adoptId, animal, userAdopt, userWorker, adoptionDate, adoptionStatus } = req.body;
    var newvalues = { $set: { adoptionStatus: 'aceite' } };
    Adoption.findByIdAndUpdate(adoptId, newvalues, { new: true }, function (err, ado) {
      Animal.findByIdAndUpdate(ado.animal, { $set: { adopted: true } }, { new: true }, function (err, animal) {


        if (err) {
          req.flash(
            'error_msg',
            'Erro a atualizar a adoção'
          );
          res.redirect('/adoption/adoptionList');
        } else {
          req.flash(
            'success_msg',
            'Adoção aceite com sucesso'
          );
          sgMail.setApiKey('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');
          const msg = {
            to: ado.userAdopt,
            from: 'noreply@quintamiao.com',
            subject: 'Pedido de Adoção Atualizado',
            html: 'Informamos que o seu pedido de adoção foi atualizado para Aceite!<br>Brevemente irá receber outro e-mail quando o seu animal estiver pronto para fazer parte da sua família! <br> Se quiser cancelar o pedido de adoção deverá contactar a Quinta do Mião. <br><br><br>RELEMBRAMOS NÃO COMPRE, ADOPTE<br><u>QUINTA DO MIÃO</u>',
          };
          sgMail.send(msg);
          res.redirect('/adoption/adoptionList');
        }
      });
    });
  } else {
    res.redirect('/denied');
  }
});

//update status denied adoption
router.post('/update/deny', function (req, res) {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
    const { adoptId, animal, userAdopt, userWorker, adoptionDate, adoptionStatus } = req.body;
    var newvalues = { $set: { adoptionStatus: 'rejeitada' } };
    Adoption.findByIdAndUpdate(adoptId, newvalues, { new: true }, function (err, ado) {
      if (err) {
        req.flash(
          'error_msg',
          'Erro a atualizar a adoção'
        );
        res.redirect('/adoption/adoptionList');
      } else {
        req.flash(
          'success_msg',
          'Adoção rejeitada com sucesso'
        );
        sgMail.setApiKey('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');
        const msg = {
          to: ado.userAdopt,
          from: 'noreply@quintamiao.com',
          subject: 'Pedido de Adoção Atualizado',
          html: 'Informamos que infelizmente o seu pedido de adoção foi rejeitado.<br><br> Para informações adicionais deverá contactar a Quinta do Mião. <br><br><br>RELEMBRAMOS NÃO COMPRE, ADOPTE<br><u>QUINTA DO MIÃO</u>',
        };
        sgMail.send(msg);
        res.redirect('/adoption/adoptionList');
      }
    });
  } else {
    res.redirect('/denied');
  }
});

//update status analysis adoption
router.post('/update/analysis', function (req, res) {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
    const { adoptId, animal, userAdopt, userWorker, adoptionDate, adoptionStatus } = req.body;
    var newvalues = { $set: { adoptionStatus: 'analise', userWorker: req.user.id } };
    Adoption.findByIdAndUpdate(adoptId, newvalues, { new: true }, function (err, ado) {

      if (err) {
        req.flash(
          'error_msg',
          'Erro a atualizar a adoção'
        );
        res.redirect('/adoption/adoptionList');
      } else {
        req.flash(
          'success_msg',
          'Adoção em análise'
        );

        sgMail.setApiKey('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');
        const msg = {
          to: ado.userAdopt,
          from: 'noreply@quintamiao.com',
          subject: 'Pedido de Adoção Atualizado',
          html: 'Informamos que o seu pedido de adoção foi atualizado para "Em Análise".<br>Brevemente irá receber outro e-mail quando o seu pedido for aceite ou rejeitado! <br> Se quiser cancelar o pedido de adoção deverá contactar a Quinta do Mião. <br><br><br>RELEMBRAMOS NÃO COMPRE, ADOPTE<br><u>QUINTA DO MIÃO</u>',
        };
        sgMail.send(msg);
        res.redirect('/adoption/adoptionList');

      }
    });
  } else {
    res.redirect('/denied');
  }
});

//update status waiting adoption
router.post('/update/waiting', function (req, res) {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
    const { adoptId, animal, userAdopt, userWorker, adoptionDate, adoptionStatus } = req.body;
    var newvalues = { $set: { adoptionStatus: 'em espera' } };
    Adoption.findByIdAndUpdate(adoptId, newvalues, { new: true }, function (err, ado) {
      if (err) {
        req.flash(
          'error_msg',
          'Erro a atualizar a adoção'
        );
        res.redirect('/adoption/adoptionList');
      } else {
        req.flash(
          'success_msg',
          'Adoção atualizada: à espera de levantamento do animal'
        );
        sgMail.setApiKey('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');
        const msg = {
          to: ado.userAdopt,
          from: 'noreply@quintamiao.com',
          subject: 'Pedido de Adoção Atualizado',
          html: 'Informamos que o seu pedido de adoção foi atualizado para "Em Espera"!<br> Isto significa que o seu animal está à espera que o vá buscar nas nossas instalações. Basta aparecer, fornecer os seus dados (nome e cartão de cidadão) e levar o seu novo amigo para casa! <br> Se quiser cancelar o pedido de adoção deverá contactar a Quinta do Mião. <br><br><br>RELEMBRAMOS NÃO COMPRE, ADOPTE<br><u>QUINTA DO MIÃO</u>',
        };
        sgMail.send(msg);
        res.redirect('/adoption/adoptionList');
      }
    });
  } else {
    res.redirect('/denied');
  }
});

//update status completed adoption
router.post('/update/completed', function (req, res) {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
    const { adoptId, animal, userAdopt, userWorker, adoptionDate, adoptionStatus } = req.body;
    var newvalues = { $set: { adoptionStatus: 'realizada' } };
    Adoption.findByIdAndUpdate(adoptId, newvalues, { new: true }, function (err, ado) {
      if (err) {
        req.flash(
          'error_msg',
          'Erro a atualizar a adoção'
        );
        res.redirect('/adoption/adoptionList');
      } else {
        req.flash(
          'success_msg',
          'Adoção realizada.'
        );
        sgMail.setApiKey('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');
        const msg = {
          to: ado.userAdopt,
          from: 'noreply@quintamiao.com',
          subject: 'Pedido de Adoção Atualizado',
          html: 'Informamos que o seu pedido de adoção foi atualizado para "Em Análise".<br>Brevemente irá receber outro e-mail quando o seu pedido for aceite ou rejeitado! <br> Se quiser cancelar o pedido de adoção deverá contactar a Quinta do Mião. <br><br><br>RELEMBRAMOS NÃO COMPRE, ADOPTE<br><u>QUINTA DO MIÃO</u>',
        };
        sgMail.send(msg);
        res.redirect('/adoption/adoptionList');
      }
    });
  } else {
    res.redirect('/denied');
  }
});

//update status canceled adoption
router.post('/update/canceled', function (req, res) {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
    const { adoptId, animal, userAdopt, userWorker, adoptionDate, adoptionStatus } = req.body;
    var newvalues = { $set: { adoptionStatus: 'cancelada' } };
    Adoption.findByIdAndUpdate(adoptId, newvalues, { new: true }, function (err, ado) {
      if (err) {
        req.flash(
          'error_msg',
          'Erro a atualizar a adoção'
        );
        res.redirect('/adoption/adoptionList');
      } else {
        req.flash(
          'success_msg',
          'Adoção cancelada'
        );
        sgMail.setApiKey('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');
        const msg = {
          to: ado.userAdopt,
          from: 'noreply@quintamiao.com',
          subject: 'Pedido de Adoção Atualizado',
          html: 'Informamos que o seu pedido de adoção foi atualizado para Aceite!<br>Brevemente irá receber outro e-mail quando o seu animal estiver pronto para fazer parte da sua família! <br> Se quiser cancelar o pedido de adoção deverá contactar a Quinta do Mião. <br><br><br>RELEMBRAMOS NÃO COMPRE, ADOPTE<br><u>QUINTA DO MIÃO</u>',
        };
        sgMail.send(msg);
        res.redirect('/adoption/adoptionList');
      }
    });
  } else {
    res.redirect('/denied');
  }
});



// show adoption details page
router.get('/adoptionDetails/:id', function (req, res) {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
    Adoption.findById({ _id: req.params.id }).then(adopt => {
      User.findOne({ email: adopt.userAdopt }, function (err, user) {
        var username = user.name;
        Animal.findOne({ _id: adopt.animal }, function (err, animal) {
          var animalName = animal.name;
          res.render('adoption/adoptionDetails', { user: req.user, adopt: adopt, adoptDate: new Date(adopt.adoptionDate).toISOString().substring(0, 10), username: username, animalName: animalName });
        });
      });
    });
  } else {
    res.redirect('/denied');
  }
})

//show create pre-conditions page
router.get('/adoptionPreCondition/:id', function (req, res) {
  if (req.user != null) {
    Animal.findOne({ _id: req.params.id }).then(animal => {
      res.render('adoption/adoptionPreConditionCreate', { user: req.user, animal: animal });
    })
  }
  else {
    req.flash(
      'success_msg',
      'Precisa de estar com o login efetuado'
    );
    res.redirect('/users/login');
  }

});

// Create Pre condition Form
router.post('/createPreCondition', (req, res) => {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario" || req.user.role == "utilizador")) {
    const { animal, userAdopt, cc, job, moreAnimals, howMany, species, money, houseType,
      manyRooms, houseSpace, outerspace, familyPersons, travel, changeHouse, responsability } = req.body;
    let errors = [];
    if (!animal || !userAdopt || !cc || !job || !moreAnimals || !money || !houseType || !manyRooms || !houseSpace || !outerspace || !familyPersons || !travel || !changeHouse || !responsability) {
      errors.push({ msg: 'Por favor preencha todos os campos obrigatórios' });
    }

    if (errors.length > 0) {
      res.render('adoption/adoptionPreConditionCreate', {
        errors,
        animal, userAdopt, cc, job, moreAnimals, howMany, species, money, houseType,
        manyRooms, houseSpace, outerspace, familyPersons, travel, changeHouse, responsability,
        user: req.user, minDate: new Date().toISOString().substring(0, 10)
      });
    } else {
      const newPreCondition = new AdoptionPreConditions({
        animal, userAdopt, cc, job, moreAnimals, howMany, species, money, houseType,
        manyRooms, houseSpace, outerspace, familyPersons, travel, changeHouse, responsability
      });

      newPreCondition
        .save()
        .then(preCondition => {
          const newAdoption = new Adoption({ animal: animal, userAdopt: userAdopt, userWorker: null, status: false, adoptionDate: new Date(), adoptionStatus: "pendente", preform: preCondition._id });
          newAdoption.save();
          req.flash(
            'success_msg',
            'Adoção realizada com sucesso'
          );
          sgMail.setApiKey('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');
          const msg = {
            to: req.user.email,
            from: 'noreply@quintamiao.com',
            subject: 'Pedido de Adoção submetido com sucesso',
            html: '<img src="../public/images/logotipo.png" alt="logo"><br><br>O seu formulário foi submetido com sucesso.<br>Em breve irá obter uma resposta ao seu pedido de adoção.<br><br>RELEMBRAMOS NÃO COMPRE, ADOPTE<br><u>QUINTA DO MIÃO</u>',
          };
          sgMail.send(msg);
          req.flash('success_msg', 'Formulário de pré-condiçoes submetido com sucesso')
          res.redirect('/');
        })
        .catch(err => console.log(err));
      // }
    }//);
  }
  else {
    res.redirect('/denied');
  }
});

router.get('/adoptionPreConditionDetails/:id', function (req, res) {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
    AdoptionPreConditions.findById({ _id: req.params.id }, function (err, adoptform) {
      Adoption.findOne({ userAdopt: adoptform.userAdopt, animal: adoptform.animal }, function (err, adoption) {
        User.findOne({ email: adoption.userAdopt }, function (err, user) {
          var username = user.name;
          Animal.findOne({ _id: adoption.animal }, function (err, animal) {
            var animalName = animal.name;
            res.render('adoption/adoptionPreConditionDetails', { user: req.user, adopt: adoption, adoptform: adoptform, username: username, animalName: animalName });
          });
        });

      });
    });
  } else {
    res.redirect('/denied');
  }
});


router.get('/adoptionStatus', function (req, res) {
  if (req.user != null) {
    Adoption.find({}, function (err, adoptions) {
      var adoptionsMap = [];
      var animalName = ''
      adoptions.forEach(function (adoption) {
        if (adoption.userAdopt === req.user.email) {
          console.log('user na area');
          Animal.findOne({ _id: adoption.animal }, function (er, animal) {
            animalName = animal.name
            console.log(animal.name);
            adoptionsMap.push({ adoption: adoption, animalName: animalName });
          })

        }
      });
      setTimeout(() => {
        res.render('adoption/adoptionStatus', { 'list': adoptionsMap, adoption: req.adoption, user: req.user, animalName: animalName });
      }, 2000);
    });
  } else {
    res.redirect('/denied');
  }
});

router.post('/filtro', function (req, res) {
  if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
    const { filter1, filter2 } = req.body;
    if (filter1 == '') {
      res.redirect('/adoption/adoptionList');
    } else {
      var dataFilter1 = new Date(filter1);
      if (filter2 == '') {
        var dataFilter2 = new Date();
      } else {
        var dataFilter2 = new Date(filter2);
      }
    }
    if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
      var adoptMap = [];
      Adoption.find({}, function (err, adoptions) {

        adoptions.forEach(function (adopt) {
          var dataado = adopt.adoptionDate;
          if (dataFilter1 < dataado && dataado < dataFilter2) {
            var ado = { adopt: adopt, animalName: null, userAdoptName: null };
            Animal.findOne({ _id: adopt.animal }, function (err, animal) {
              ado.animalName = animal.name;
            });

            User.findOne({ email: adopt.userAdopt }, function (err, user) {
              ado.userAdoptName = user.name;
            });
            adoptMap.push(ado);
          }
        });
        setTimeout(function () {
          res.render('adoption/adoptionList', { 'list': adoptMap, user: req.user });
        }, 220 * adoptions.length);
      });
    }
  } else {
    res.redirect('/denied');
  }
});

module.exports = router;