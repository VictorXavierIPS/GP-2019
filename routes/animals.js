const express = require('express');
const router = express.Router();
const fileUpload = require('express-fileupload');

// Load Animal model
const Animal = require('../models/animal');
const Breed = require('../models/breed');
const Age = require('../config/age');

router.use(fileUpload());

//Create Animal Record Page
router.get('/createAnimalReport', (req, res) => {
    if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
        Breed.find({}, function (err, breeds) {
            var breedMap = [];
            breeds.forEach(function (breed) {
                breedMap.push(breed);
            });
            res.render('animal/createAnimalReport', { 'list': breedMap, breed: req.breed, user: req.user });
        });
    } else {
        res.redirect('/denied');
    }
});

//Create Animal Record 
router.post('/createAnimalReport', (req, res) => {
    if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
        const { fur, size, breed, adopted, birthdate, name, gender, description, behavior, sociability, dogFriendly, catFriendly, childFriendly, inteligent, trainable, watchdog } = req.body;
        let errors = [];

        if (errors.length > 0) {
            res.render('animal/createAnimalReport', {
                errors,
                fur,
                size,
                breed,
                adopted: false,
                birthdate,
                name,
                gender,
                description,
                behavior,
                sociability,
                dogFriendly,
                catFriendly,
                childFriendly,
                inteligent,
                trainable,
                watchdog,
                user: req.user
            });
        } else {
            var base64String;
            if (req.files != null) {
                var imageFile = req.files.animalPicture;
                base64String = imageFile.data.toString('base64');
            }
            var newAnimal = new Animal({
                fur,
                size,
                breed,
                adopted: false,
                birthdate,
                name,
                gender,
                description,
                behavior,
                sociability,
                dogFriendly,
                catFriendly,
                childFriendly,
                inteligent,
                trainable,
                watchdog,
                picture: base64String,
                likedUsers: []
            });
            newAnimal
                .save()
                .then(user => {
                    res.redirect('/animals/animalsList');
                    console.log("Ficha criada com sucesso!");
                })

        }

    } else {
        res.redirect('/denied');
    }
});

//Detail Animal Report Page
router.get('/detail/:id', function (req, res) {
    Animal.findOne({ _id: req.params.id }).then(animal => {
        animal.age = Age.dogAge(animal.birthdate);
        var like = false
        if (req.user != null) {
            for (let index = 0; index < animal.likedUsers.length; index++) {
                if (animal.likedUsers[index] == req.user.id) {
                    like = true
                }
                console.log(like);
            }
            console.log(like);
            res.render('animal/detailAnimalReport', {
                user: req.user,
                animal: animal,
                like: like
            });
        } else {
            res.render('animal/detailAnimalReport', {
                user: req.user,
                animal: animal,
                like: null
            });
        }
    });
});

//Add Cuteness Point 
router.post('/like', (req, res) => {
    if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario" || req.user.role == "utilizador")) {
        const { id } = req.body;
        Animal.findOne({ _id: id }, function (error, animal) {
            var array = animal.likedUsers;
            array.push(req.user._id);

            Animal.findByIdAndUpdate(id, { $set: { likedUsers: array } }, { new: true }, function (err, res) {
                if (err) throw err;
            })
            res.redirect('detail/' + id);
        });
    }
    else {
        res.redirect('/denied');
    }
});

//Remove Cuteness Point
router.post('/dislike', (req, res) => {
    if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario" || req.user.role == "utilizador")) {
        const { id } = req.body;
        Animal.findOne({ _id: id }, function (error, animal) {
            var array = animal.likedUsers;
            for (let i = 0; i < array.length; i++) {
                if (array[i] == req.user.id) {
                    array.splice(i, 1);
                }
            }
            Animal.findByIdAndUpdate(id, { $set: { likedUsers: array } }, { new: true }, function (err, res) {
                if (err) throw err;
            })
            res.redirect('detail/' + id);
        });
    }
    else {
        res.redirect('/denied');
    }
});



//Edit Animal Report Page
router.get('/edit/:id', (req, res) => {
    if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
        Animal.findById({ _id: req.params.id }).then(animal => {
            Breed.find({}, function (err, breeds) {
                var breedMap = [];
                breeds.forEach(function (breed) {
                    breedMap.push(breed);
                });
                res.render('animal/editAnimalReport', {
                    'list': breedMap,
                    breed: req.breed,
                    user: req.user,
                    animal: animal
                });
            });
        });
    } else {
        res.redirect('/denied');
    }
});

//Edit Animal Report
router.post('/edit/', (req, res) => {
    if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
        var newvalues;
        var animalUpdated;
        console.log(req.files)
        if (req.files != null) {
            var imageFile = req.files.animalPicture;
            const base64String = imageFile.data.toString('base64');
            newvalues = { $set: { name: req.body.name, birthdate: req.body.birthdate, breed: req.body.breed, gender: req.body.gender, size: req.body.size, fur: req.body.fur, adopted: req.body.adopted, description: req.body.description, picture: base64String, behavior: req.body.behavior, sociability: req.body.sociability } };
            animalUpdated = { name: req.body.name, birthdate: req.body.birthdate, breed: req.body.breed, gender: req.body.gender, size: req.body.size, fur: req.body.fur, adopted: req.body.adopted, description: req.body.description, picture: base64String, behavior: req.body.behavior, sociability: req.body.sociability };
        } else {
            newvalues = { $set: { name: req.body.name, birthdate: req.body.birthdate, breed: req.body.breed, gender: req.body.gender, size: req.body.size, fur: req.body.fur, adopted: req.body.adopted, description: req.body.description, behavior: req.body.behavior, sociability: req.body.sociability } };
            animalUpdated = { name: req.body.name, birthdate: req.body.birthdate, breed: req.body.breed, gender: req.body.gender, size: req.body.size, fur: req.body.fur, adopted: req.body.adopted, description: req.body.description, behavior: req.body.behavior, sociability: req.body.sociability };
        }
        Animal.findByIdAndUpdate(req.body.id, newvalues, { new: true }, function (err, res) {
            if (err) throw err;
            console.log(req.body);

        });

        Animal.find({}, function (err, animals) {
            var animalMap = [];
            animals.forEach(function (animal) {
                animal.age = Age.age(animal.birthdate);
                animalMap.push(animal);
            });


            res.render('animal/animalsList', { 'list': animalMap, animal: animalUpdated, user: req.user });
            req.flash('success_msg', 'As suas alterções foram salvas');
        });
    } else {
        res.redirect('/denied');
    }
});


//Delete Animal Report
router.get('/delete/:id', function (req, res) {
    if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
        Animal.findByIdAndRemove({ _id: req.params.id }).then(animal => {
            console.log(req.params.id);
            req.flash(
                'success_msg',
                'Ficha eliminada com sucesso'
            );
            res.redirect('/animals/animalsList');
        });
    } else {
        res.redirect('/denied');
    }
});

//Animals to adopt list
router.get('/animalsToAdoptList', function (req, res) {
    Animal.find({}, function (err, animals) {
        var animalMap = [];
        animals.forEach(function (animal) {
            if (animal.adopted == false) {
                animal.age = Age.dogAge(animal.birthdate);
                animalMap.push(animal);

            }
        });
        res.render('animal/animalsToAdoptList', { 'list': animalMap, animal: req.animal, user: req.user });
    });
});

//Animals list
router.get('/animalsList', function (req, res) {
    if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
        Animal.find({}, function (err, animals) {
            var animalMap = [];
            animals.forEach(function (animal) {
                animal.age = Age.dogAge(animal.birthdate);
                animalMap.push(animal);
            });
            res.render('animal/animalsList', { 'list': animalMap, animal: req.animal, user: req.user });
        });
    } else {
        res.redirect('/denied');
    }
});

router.post('/sortLikes', function (req, res) {
    if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
        Animal.find({}, function (err, animals) {
            var animalMap = [];
            animals.forEach(function (animal) {
                animal.age = Age.dogAge(animal.birthdate);
                animalMap.push(animal);
            });

            animalMap.sort(function (a, b) {
                if (a.likedUsers.length > b.likedUsers.length) {
                    return -1;
                }
                if (a.likedUsers.length < b.likedUsers.length) {
                    return 1;
                }
                // a must be equal to b
                return 0;
            })

            res.render('animal/animalsList', { 'list': animalMap, animal: req.animal, user: req.user });
        });

    } else {

        res.redirect('/denied');
    }
});

router.post('/sortLikesToAdopt', function (req, res) {
    Animal.find({}, function (err, animals) {
        var animalMap = [];
        animals.forEach(function (animal) {
            animal.age = Age.dogAge(animal.birthdate);
            if (animal.adopted == false) {
                animalMap.push(animal);
            }

        });

        animalMap.sort(function (a, b) {
            if (a.likedUsers.length > b.likedUsers.length) {
                return -1;
            }
            if (a.likedUsers.length < b.likedUsers.length) {
                return 1;
            }
            // a must be equal to b
            return 0;
        })
        res.render('animal/animalsToAdoptList', { 'list': animalMap, animal: req.animal, user: req.user });
    });

});

router.post('/filter', function (req, res) {
    if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
        const { filter,
            input } = req.body
        Animal.find({}, function (err, animals) {
            var animalMap = [];
            console.log(filter);

            console.log(input);
            if (filter == 'name') {
                animals.forEach(function (animal) {
                    if (input == animal.name) {
                        animal.age = Age.dogAge(animal.birthdate);
                        animalMap.push(animal);
                    };
                });
            }
            if (filter == 'raca') {
                animals.forEach(function (animal) {
                    if (input == animal.breed) {
                        animal.age = Age.dogAge(animal.birthdate);
                        animalMap.push(animal);
                    }
                });
            }
            res.render('animal/animalsList', { 'list': animalMap, animal: req.animal, user: req.user });
        });
    } else {
        res.redirect('/denied');
    };
});

router.post('/filterToAdopt', function (req, res) {
    const { filter,
        input } = req.body
    Animal.find({}, function (err, animals) {
        var animalMap = [];
        console.log(filter);

        console.log(input);
        if (filter == 'name') {
            animals.forEach(function (animal) {
                if (input == animal.name) {
                    animal.age = Age.dogAge(animal.birthdate);
                    if (animal.adopted == false) {
                        animalMap.push(animal);
                    }
                };
            });
        }
        if (filter == 'raca') {
            animals.forEach(function (animal) {
                if (input == animal.breed) {
                    animal.age = Age.dogAge(animal.birthdate);
                    if (animal.adopted == false) {
                        animalMap.push(animal);
                    }
                }
            });
        }
        res.render('animal/animalsToAdoptList', { 'list': animalMap, animal: req.animal, user: req.user });
    });
});
//Adopted Animals List
router.get('/adoptedAnimalsList', function (req, res) {
    if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario")) {
        Animal.find({}, function (err, animals) {
            var animalMap = [];
            animals.forEach(function (animal) {
                if (animal.adopted == true) {
                    animal.age = Age.dogAge(animal.birthdate);
                    animalMap.push(animal);

                }
            });
            res.render('animal/adoptedAnimalsList', { 'list': animalMap, animal: req.animal, user: req.user });
        });
    } else {
        res.redirect('/denied');
    }
});

//Liked Animals List
router.get('/animalsFaves', function (req, res) {
    if (req.user != null && (req.user.role == "admin" || req.user.role == "funcionario" || req.user.role == "utilizador")) {
        var user = req.user.id;
        Animal.find({}, function (err, animals) {
            var animalMap = [];
            animals.forEach(function (animal) {
                animal.likedUsers.forEach(function (like) {
                    if (like == user) {
                        animalMap.push(animal);
                    }
                });
            });
            res.render('animal/favesList', { 'list': animalMap, user: req.user });
        });
    } else {
        res.redirect('/denied');
    }
});

module.exports = router;
