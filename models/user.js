const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const age = require('../config/age');

const role = {
  ADMIN: "admin",
  WORKER: "funcionario",
  USER: "utilizador"
}

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  birthdate: {
    type: Date,
    required: false
  },
  address: {
    type: String,
    required: false
  },
  phone: {
    type: Number,
    required: false
  },
  confirmed: {
    type: Boolean,
    required: false,
    value: false
  },
  role: {
    type: role,
    required: true
  }
});

/**
 * This function will create the user and save it in the Database checking for errors from the parameters and encripting the password.
 * 
 * @param {String} name 
 * @param {String} email 
 * @param {Date} birthdate 
 * @param {String} address 
 * @param {Number} phone 
 * @param {role} role 
 * @param {String} password 
 */
function createUser(name, email, birthdate, address, phone, role, password) {
  let errors = [];

  if (!name || !email || !birthdate || !password) {
    errors.push({ msg: 'Por favor preencha todos os campos obrigatórios' });
  }
  if (phone !== '' && phone.length < 9) {
    errors.push({ msg: 'Telemóvel deve ter no mínimo 9 dígitos' });
  }
  if (age.age(birthdate) < 16) {
    errors.push({ msg: 'Deve ter mais de 16 anos para se poder registar' });
  }
  if (password.length < 6) {
    errors.push({ msg: 'Palavra-passe deve ter pelo menos 6 caracteres' });
  }

  if (errors.length > 0) {
    return errors;
  }
  const user = new User({
    name,
    email,
    password,
    birthdate,
    address,
    phone,
    role
  });

  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) throw err;
      user.password = hash;
      user.save();
    });
  });
}

/**
 * This function will search for a user and update with the new values.
 * 
 * @param {String} email 
 * @param {String} name 
 * @param {Date} birthdate 
 * @param {String} address 
 * @param {Number} phone 
 */
function updateUser(email, name, birthdate, address, phone) {
  User.findOne({ email: email }).then(user => {
    var newvalues = { $set: { name: name, birthdate: birthdate, address: address, phone: phone } };

    User.findOneAndUpdate({ email: email }, newvalues, { new: true }, function (err, res) {
      if (err) throw err;
    });
  });
}

/**
 * This function will delete a user with the matching email address
 * 
 * @param {String} email 
 */
function deleteUser(email){
  User.findOneAndDelete({email: email}, function(err, res){
    if (err) return 'User not found to delete';
  });
}

const User = mongoose.model('User', UserSchema);


module.exports = User;
module.exports.create = createUser;
module.exports.update = updateUser;
module.exports.delete = deleteUser;