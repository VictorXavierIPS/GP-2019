const mongoose = require('mongoose');
const User = require('../models/user');


const status = {

    //em análise pelos funcionários
    ANALYSIS: "analise",
    //pendente
    Pending: "pendente",
    //adocao aceite
    ACCEPTED: "aceite",
    //adocao rejeitada
    DENIED: "rejeitada",
    //à espera que vão buscar o animal
    WAITING: "em espera",
    //Adoção bem sucedida
    COMPLETED: "realizada",
    //Adocao cancelada
    CANCELED: "cancelada"

  }

const AdoptionSchema = new mongoose.Schema({
    
    animal:{
        type:String,
        required: true
    },
    userAdopt:{
        type: String,
        required: true
    },
    userWorker:{
        type:String,
        required: false
    },
    adoptionDate:{
        type: Date,
        required:true
    },
    adoptionStatus:{
        type: status,
        required:false
    },
    preform:{
        type:String,
        required:false
    }
  

});

const Adoption = mongoose.model('Adoption', AdoptionSchema);

module.exports = Adoption;
