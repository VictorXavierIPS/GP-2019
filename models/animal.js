const mongoose = require('mongoose');
const User = require('./user');
const Breed = require('./breed');

const fur = {
    SMALL: "small",
    MEDIUM: "medium",
    LONG: "long"
}

const size = {
    SMALL: "small",
    MEDIUM: "medium",
    LONG: "large"
}

const gender = {
    FEMALE: "female",
    MALE: "male"
}

const behavior = {
    VERYACTIVE: "veryactive",
    ACTIVE: "active",
    CALM: "calm",
    VERYCALM: "verycalm",
    PLAYFUL: "playful"
}

const sociability = {
    SOCIABLE: "sociable",
    SHY: "shy",
    AGGRESSIVE: "aggressive"
}

const AnimalSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    birthdate: {
        type: Date,
        required: true,
    },
    breed: {
        type: String,
        required: true
    },
    gender: {
        type: gender,
        required: true,
    },
    fur: {
        type: fur,
        required: true
    },
    size: {
        type: size,
        required: true
    },
    adopted: {
        type: Boolean,
        required: false,
        value: false
    },
    adopter: {
        type: String,
        required: false,
    },
    description: {
        type: String,
        required: false,
    },
    likedUsers: {
        type: Array,
        required: false
    },
    behavior: {
        type: behavior,
        required: false
    },
    sociability: {
        type: sociability,
        required: false
    },
    picture: {
        type: String,
        required: false
    },
    dogFriendly: {
        type: Boolean,
        required: false
    },
    catFriendly: {
        type: Boolean,
        required: false
    },
    childFriendly: {
        type: Boolean,
        required: false
    },
    inteligent: {
        type: String,
        required: false
    },
    trainable: {
        type: String,
        required: false
    },
    watchdog: {
        type: Boolean,
        required: false
    }
});

/**
 * This function will create an animal for adoption, acording to the parameter values.
 * 
 * @param {fur} fur 
 * @param {size} size 
 * @param {String} breed 
 * @param {Date} birthdate 
 * @param {String} name 
 * @param {gender} gender 
 * @param {String} description 
 * @param {behavior} behavior 
 * @param {sociability} sociability 
 * @param {Boolean} dogFriendly 
 * @param {Boolean} catFriendly 
 * @param {Boolean} childFriendly 
 * @param {String} inteligent 
 * @param {String} trainable 
 * @param {Boolean} watchdog 
 * @param {String} picture 
 */
function createAnimal(fur, size, breed, birthdate, name, gender, description, behavior, sociability, dogFriendly, catFriendly, childFriendly, inteligent, trainable, watchdog, picture) {
    var base64String;
    if (picture != null) {
        base64String = picture.data.toString('base64');
    }
    var newAnimal = new Animal({
        fur,
        size,
        breed,
        adopted: false,
        birthdate,
        name,
        gender,
        description,
        behavior,
        sociability,
        dogFriendly,
        catFriendly,
        childFriendly,
        inteligent,
        trainable,
        watchdog,
        picture: base64String,
        likedUsers: []
    });
    newAnimal.save();
}

/**
 * This function will get the animal from the database with the matching ID and will be updated with the new values
 * 
 * @param {String} id 
 * @param {fur} fur 
 * @param {size} size 
 * @param {String} breed 
 * @param {Boolean} adopted 
 * @param {Date} birthdate 
 * @param {String} name 
 * @param {gender} gender 
 * @param {String} description 
 * @param {behavior} behavior 
 * @param {sociability} sociability 
 * @param {Boolean} dogFriendly 
 * @param {Boolean} catFriendly 
 * @param {Boolean} childFriendly 
 * @param {String} inteligent 
 * @param {String} trainable 
 * @param {Boolean} watchdog 
 * @param {String} picture 
 */
function updateAnimal(id, fur, size, breed, adopted, birthdate, name, gender, description, behavior, sociability, dogFriendly, catFriendly, childFriendly, inteligent, trainable, watchdog, picture) {
    var newvalues = { $set: { fur: fur, size: size, breed: breed, adopted: adopted, birthdate: birthdate, name: name, gender: gender, description: description, behavior: behavior, sociability: sociability, dogFriendly: dogFriendly, catFriendly: catFriendly, childFriendly: childFriendly, inteligent: inteligent, trainable: trainable, watchdog: watchdog, picture: picture } };

    Animal.findByIdAndUpdate(id, newvalues, { new: true }, function (err, res) {
    if (err) throw 'Animal not found to update';
    });
}
  
/**
 * This function will find an animal with the matching ID to be deleted
 * 
 * @param {String} id 
 */
function deleteAnimal(id){
    Animal.findByIdAndDelete(id, function(err, res){
        if (err) return 'Animal not found to delete';
    });
}

const Animal = mongoose.model('Animal', AnimalSchema);

module.exports = Animal;
module.exports.create = createAnimal;
module.exports.update = updateAnimal;
module.exports.delete = deleteAnimal;
