const mongoose = require('mongoose');

const AdoptionPreConditionsSchema = new mongoose.Schema({
    
    animal:{
        type:String,
        required: true
    },
    userAdopt:{
        type: String,
        required: true
    },
    cc:{
        type:Number,
        required:true
    },
    job:{
        type:String,
        required:true
    },
    moreAnimals:{
        type:Boolean,
        required:true
    },
    howMany:{
        type:Number,
        required: false
    },
    species:{
        type:String,
        required:false
    },
    money:{
        type:Boolean,
        required:true
    },
    houseType:{
        type:String,
        required:true
    },
    manyRooms:{
        type:Number,
        required:true
    },
    houseSpace:{
        type:Number,
        required:true
    },
    outerspace:{
        type:Boolean,
        required:true
    },
    familyPersons:{
        type:Number,
        required:true
    },
    travel:{
        type:String,
        required:true
    },
    changeHouse:{
        type:String,
        required:true
    },
    responsability:{
        type:Boolean,
        required:true
    }

});

const AdoptionPreConditions = mongoose.model('AdoptionPreConditions', AdoptionPreConditionsSchema);

module.exports = AdoptionPreConditions;
