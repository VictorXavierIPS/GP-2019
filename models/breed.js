const mongoose = require('mongoose');

const temperment = {
    AFFECTIONATE: "affectionate",
    ALERT: "alert",
    CHEERFUL: "cheerful",
    CONFIDENT: "confident",
    COURAGEOUS:"courageous",
    DOCILE: "docile",
    ENERGETIC: "energetic",
    FRIENDLY: "friendly",
    GENTLE: "gentle",
    INTELLIGENT: "intelligent",
    KIND:"kind",
    LOVING: "loving",
    LOYAL: "loyal",
    OUTGOING: "outgoing",
    PATIENT: "patient",
    PLAYFUL: "playful",
    RESPONSIVE: "responsive",
    SOCIAL: "social",
    TRAINABLE: "trainable"
}

const size = {
    SMALL: "small",
    MEDIUM: "medium",
    LONG: "large"
}

const group = {
    TOY: "toy",
    SPORTING: "sporting",
    HOUND: "hound",
    TERRIER: "terrier",
    WORKING: "working",
    HERDING: "herding"
}

const characteristics = {
    HYPOALLERGENIC: "hypoallergenic",
    FLUFFY: "fluffy",
    BESTFAMILY: "bestefamily",
    SMARTEST: "smartest",
    BESTGUARD: "bestguard",
    KIDFRIENDLY: "kidfriendly",
    BESTWATCH: "bestwatch",
    EASYTOTRAIN: "easytotrain"
}

const lifeSpam = {
    SHOT: "short",
    MEDIUM: "medium",
    LONG: "long"
}

const color = {
    BLACK: "black",
    CHOCOLATE: "chocolate",
    YELLOW: "yellow",
    CREAM: "cream",
    WHITE: "white",
    TRICOLOR: "tricolor",
    RED: "red",
}
const BreedSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    group: {
        type: group,
        required: false
    },
    size: {
        type: size,
        required: false
    },
    lifeSpam: {
        type: lifeSpam,
        required: false
    },
    temperment: {
        type: temperment,
        required: false
    },
    characteristics: {
        type: characteristics,
        required: false
    },
    color: {
        type: color,
        required: false
    },
});

const Breed = mongoose.model('Breed', BreedSchema);

module.exports = Breed;
