module.exports = {
    age: function calculate_age(birthdate) {
        var birth = new Date(birthdate)
        var diff_ms = Date.now() - birth.getTime();
        var age_dt = new Date(diff_ms);
        return Math.abs(age_dt.getUTCFullYear() - 1970);
    },
    dogAge: function getAge(DOB) {
        var today = new Date();
        var birthDate = new Date(DOB);
        var age = today.getFullYear() - birthDate.getFullYear();
        var day = today.getDay() - birthDate.getDay();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age = age - 1;
        }

        if (age == 0) {
            if (m == 0) {
                if (day == 1) {
                    return day + " dia";
                } else {
                    return day + " dias";
                }
            } else if (m == 1)   {
                return m + " mês";
            } if (m > 1) {
                return m + " meses";
            }
        } else if (age == 1)
            return age + " ano";
        else {
            return age + " anos"
        }
    }
};
